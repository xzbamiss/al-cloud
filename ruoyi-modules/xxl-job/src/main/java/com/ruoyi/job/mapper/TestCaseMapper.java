package com.ruoyi.job.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.job.entity.TestCase;

import java.util.List;

/**
 * 测试用例Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface TestCaseMapper extends BaseMapper<TestCase> {



}
