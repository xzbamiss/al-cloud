/*
 * @Author: daifen
 * @Date: 2020-06-16 14:12:36
 * @LastEditors: daifen
 */ 

import request from '@/utils/request'

/**
 * @Desc: 封装的下拉框的接口
 * @Author: daifen
 * @Date: 2020-06-16 14:13:19
 * @param {type} 
 * @return: 
 */
export function getSelect() {
    return request({
      url: '/code/scheme/getAll',
      method: 'get', 
    })
  }